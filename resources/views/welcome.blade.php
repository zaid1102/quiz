@extends('layouts.app')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        {{ __('Question Listing') }}
                        <button style="float: right; font-weight: 900;" class="btn btn-info btn-sm" type="button"  data-toggle="modal" data-target="#CreateQuestionModal">
                            Create Question
                        </button>
                        <a href="{{ url('show/quiz')}}" class="btn btn-success"><i class="fa fa-plus"></i>Go to Quiz</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered datatable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Category</th>
                                    <th>Question</th>
                                    <th>Option1</th>
                                    <th>Option2</th>
                                    <th>Option3</th>
                                    <th>Option4</th>
                                    <th width="150" class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Create Question Modal -->
<div class="modal" id="CreateQuestionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Question Create</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Question was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <strong>Question Category:</strong>
                    <select class="form-control" name="category" id="category">
                        <option value="">Select Category</option>
                        <option value="Technical">Technical</option>
                        <option value="Aptitude">Aptitude</option>
                        <option value="Logical">Logical</option>
                    </select>
                </div>
                <div class="form-group">
                    <strong>Question:</strong>
                    <input type="text" name="question" class="form-control" placeholder="Question" id="question">
                </div>
                <div class="form-group">
                    <strong>Option 1:</strong>
                    <input type="text" name="option_1" class="form-control" placeholder="Option 1" id="option_1">
                </div>
                <div class="form-group">
                    <strong>Option 2:</strong>
                    <input type="text" name="option_2" class="form-control" placeholder="Option 2" id="option_2">
                </div>
                <div class="form-group">
                    <strong>Option 3:</strong>
                    <input type="text" name="option_3" class="form-control" placeholder="Option 3" id="option_3">
                </div>
                <div class="form-group">
                    <strong>Option 4:</strong>
                    <input type="text" name="option_4" class="form-control" placeholder="Option 4" id="option_4">
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateQuestionForm">Create</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Question Modal -->
<div class="modal" id="EditQuestionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Question Edit</h4>
                <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Question was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="EditQuestionModalBody">

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitEditQuestionForm">Update</button>
                <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Question Modal -->
<div class="modal" id="DeleteQuestionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Question Delete</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h4>Are you sure want to delete this Question?</h4>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="SubmitDeleteQuestionForm">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Initialize Datatable
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            "order": [[ 0, "desc" ]],
            ajax: '{{ url("question/get") }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'category', name: 'category'},
                {data: 'question', name: 'question'},
                {data: 'option1', name: 'option1'},
                {data: 'option2', name: 'option2'},
                {data: 'option3', name: 'option3'},
                {data: 'option4', name: 'option4'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });

        // Create question Ajax request.
        $('#SubmitCreateQuestionForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('question/store') }}",
                method: 'post',
                data: {
                    category: $('#category').val(),
                    question: $('#question').val(),
                    option1: $('#option_1').val(),
                    option2: $('#option_2').val(),
                    option3: $('#option_3').val(),
                    option4: $('#option_4').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){
                            $('.alert-success').hide();
                            $('#CreateQuestionModal').modal('hide');
                            location.reload();
                        }, 2000);
                    }
                }
            });
        });

        // Get single question in EditModel
        $('.modelClose').on('click', function(){
            $('#EditQuestionModal').hide();
        });
        var id;
        $('body').on('click', '#getEditQuestionData', function(e) {
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: "{{ url('question/fetch') }}"+'/'+id,
                method: 'GET',
                success: function(result) {
                    console.log(result);
                    $('#EditQuestionModalBody').html(result.html);
                    $('#EditQuestionModal').show();
                }
            });
        });

        // // Update question Ajax request.
        $('#SubmitEditQuestionForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('question/edit') }}"+'/'+id,
                method: 'PUT',
                data: {
                    category: $('#editCategory').val(),
                    question: $('#editQuestion').val(),
                    option1: $('#editOption_1').val(),
                    option2: $('#editOption_2').val(),
                    option3: $('#editOption_3').val(),
                    option4: $('#editOption_4').val(),
                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){
                            $('.alert-success').hide();
                            $('#EditQuestionModal').hide();
                        }, 2000);
                    }
                }
            });
        });

        // Delete question Ajax request.
        var deleteID;
        $('body').on('click', '#getDeleteId', function(){
            deleteID = $(this).data('id');
        })
        $('#SubmitDeleteQuestionForm').click(function(e) {
            e.preventDefault();
            var id = deleteID;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('question/delete') }}"+'/'+id,
                method: 'DELETE',
                success: function(result) {
                    setInterval(function(){
                        $('.datatable').DataTable().ajax.reload();
                        $('#DeleteQuestionModal').hide();
                    }, 1000);
                }
            });
        });
    });
</script>
@endsection
