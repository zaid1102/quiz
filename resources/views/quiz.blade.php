@extends('layouts.app')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        Multiple Choice Questions
                    </div>
                    <div>
                        @foreach ($categories as $category)
                            <h3>{{ $category->category }}</h3>
                            @foreach ($questions as $question)
                                @if ($category->category == $question->category)
                                <div>
                                    <h5>Q. {{ $question->question }}</h5>
                                    <div class="form-group">
                                        <label class="form-check-label form-control">
                                            <input class="form-check-input" type="radio" name="result" value="{{ $question->option1 }}" >
                                            {{ $question->option1 }}
                                        </label>
                                        <label class="form-check-label form-control">
                                            <input class="form-check-input" type="radio" name="result" value="{{ $question->option2 }}" >
                                            {{ $question->option2 }}
                                        </label>
                                        <label class="form-check-label form-control">
                                            <input class="form-check-input" type="radio" name="result" value="{{ $question->option3 }}" >
                                            {{ $question->option3 }}
                                        </label>
                                        <label class="form-check-label form-control">
                                            <input class="form-check-input" type="radio" name="result" value="{{ $question->option4 }}" >
                                            {{ $question->option4 }}
                                        </label>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
