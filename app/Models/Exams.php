<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Exams extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'category',
        'question',
        'option1',
        'option2',
        'option3',
        'option4',
    ];
}
