<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exams;
use Validator;

class ExamController extends Controller
{
    public function index() {
        $questions = Exams::get();
        if(count($questions) > 0) {
            $data = $questions;
        }
        else {
            $data = [];
        }
        return \DataTables::of($data)
        ->addColumn('Actions', function($data) {
            return '<button type="button" class="btn btn-success btn-sm" id="getEditQuestionData" data-id="'.$data->id.'">Edit</button>
                <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteQuestionModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
        })->rawColumns(['Actions'])->make(true);
    }

    public function show($id) {
        $data = Exams::find($id);
        $html = '<div class="form-group">
                    <strong>Question Category:</strong>
                    <select class="form-control" name="category" id="editCategory">
                        <option value="'.$data->category.'" selected>'.$data->category.'</option>
                        <option value="Technical">Technical</option>
                        <option value="Aptitude">Aptitude</option>
                        <option value="Logical">Logical</option>
                    </select>
                </div>
                <div class="form-group">
                    <strong>Question:</strong>
                    <input type="text" name="question" class="form-control" placeholder="Question" id="editQuestion" value="'.$data->question.'">
                    <label for="Name">Description:</label>
                </div>
                <div class="form-group">
                    <strong>Option 1:</strong>
                    <input type="text" name="option_1" class="form-control" placeholder="Option 1" id="editOption_1" value="'.$data->option1.'">
                </div>
                <div class="form-group">
                    <strong>Option 2:</strong>
                    <input type="text" name="option_2" class="form-control" placeholder="Option 2" id="editOption_2" value="'.$data->option2.'">
                </div>
                <div class="form-group">
                    <strong>Option 3:</strong>
                    <input type="text" name="option_3" class="form-control" placeholder="Option 3" id="editOption_3" value="'.$data->option3.'">
                </div>
                <div class="form-group">
                    <strong>Option 4:</strong>
                    <input type="text" name="option_4" class="form-control" placeholder="Option 4" id="editOption_4" value="'.$data->option4.'">
                </div>';

        return response()->json(['html'=>$html]);
    }

    public function store(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'question' => 'required',
            'option1' =>'required',
            'option2' =>'required',
            'option3' =>'required',
            'option4' =>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['response' => ['status' => 'validation error', 'detail' => $validator->errors()]], Response::HTTP_OK);
        }

        $insert = [
            'category' => ucfirst($request->category),
            'question' => $request->question,
            'cover_image' => $request->cover_image,
            'option1' => $request->option1,
            'option2' => $request->option2,
            'option3' => $request->option3,
            'option4' => $request->option4,
        ];

        //Create Book in Database
        $exam = Exams::create($insert);

        return response()->json(['success'=>'Question added successfully']);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'question' => 'required',
            'option1' =>'required',
            'option2' =>'required',
            'option3' =>'required',
            'option4' =>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $exam = Exams::find($id);

        if (isset($request->category)) {
            $exam->category = ucfirst($request->category);
        }
        if (isset($request->question)) {
            $exam->question = $request->question;
        }
        if (isset($request->option1)) {
            $exam->option1 = $request->option1;
        }
        if (isset($request->option2)) {
            $exam->option2 = $request->option2;
        }
        if (isset($request->option3)) {
            $exam->option3 = $request->option3;
        }
        if (isset($request->option4)) {
            $exam->option4 = $request->option4;
        }

        $exam->save();

        return response()->json(['success'=>'Question updated successfully']);
    }

    public function destroy($id)
    {
        $exam = Exams::find($id);
        $exam->delete();

        return response()->json(['success'=>'Question deleted successfully']);
    }

    public function showQuiz(){

        $categories = Exams::select('category')->distinct()->get();
        $questions = Exams::get();

        return view('quiz', ['categories' => $categories, 'questions' =>$questions]);
    }
}
