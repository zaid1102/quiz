<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('question/get', [ExamController::class, 'index']); /* Get All Questions */
Route::post('question/store', [ExamController::class, 'store']); /* Store Question */
Route::get('question/fetch/{id}', [ExamController::class, 'show']); /* Get Question by Id */
Route::put('question/edit/{id}', [ExamController::class, 'update']); /* Update Question */
Route::delete('question/delete/{id}', [ExamController::class, 'destroy']); /* Delete Question */
Route::get('show/quiz', [ExamController::class, 'showQuiz']); /* Show Quiz Page */
